﻿using System.Configuration;

namespace Base
{
    public class DefaultDatabase<TEntity> : Connection<TEntity> where TEntity : class
    {
        public DefaultDatabase()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["MercadIn.Properties.Settings.crudConnectionString"].ConnectionString;
                //System.Configuration.ConfigurationManager.ConnectionStrings["defaultDatabase"].ConnectionString;
        }
    }
}