﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Categoria")]
    public class Categoria : DefaultDatabase<Categoria>
    {
        public int id { get; set; }
        public string nome { get; set; }

        public string AdicionarDados(Categoria obj)
        {
            try
            {
                ExecuteQuery(@"INSERT INTO vini.D_Categoria (nome) VALUES(@nome)", new { obj.nome });
                return "Adicionado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string AtualizarDados(Categoria obj)
        {
            try
            {
                ExecuteQuery(@"UPDATE vini.D_Categoria SET nome = @nome WHERE id = @id",
                            new { obj.nome, obj.id });
                return "Atualizado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Categoria> ListarCategorias()
        {
            return ExecuteQuery<Categoria>(@"SELECT * FROM vini.D_Categoria");
        }
    }
}
