﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataBase
{
    [Table("vini.D_Pessoa")]
    public class Pessoa : DefaultDatabase<Pessoa>
    {
        public int id { get; set; }
        public string cpf { get; set; }
        public string nome { get; set; }
        public DateTime dataNascimento { get; set; }
        public string email { get; set; }
        public string senha { get; set; }

        public string message = "";

        public class VerificarAcesso 
        {
            public int id { get; set; }
            public string cpf { get; set; }
            public string nome { get; set; }
            public DateTime dataNascimento { get; set; }
            public string email { get; set; }
            public string senha { get; set; }

            public int nivelAcesso;
        }

        public VerificarAcesso VerificarAcessar(string email, string senha)
        {
            VerificarAcesso obj = ExecuteQuery<VerificarAcesso>(@"  SELECT P.*, PA.acessoId nivelAcesso FROM vini.D_Pessoa P
                                                INNER JOIN vini.D_PessoaAcesso PA ON PA.pessoaId = P.id
                                                WHERE email = @email AND senha = @senha", new { email, senha }).FirstOrDefault();
            if (obj != null)
                return obj;
            else
                return null;
        }

        public string InserirCliente(Pessoa obj)
        {
            var exist = new Pessoa().SelecionarPorCpf(obj.cpf);
            if (exist.Any())
                message = "CPF já cadastrado!";
            else
            {
                try
                {
                    ExecuteQuery(@"INSERT INTO vini.D_Pessoa (cpf,nome,dataNascimento,email,senha) VALUES (@cpf,@nome,@dataNascimento,@email,@senha)", new { obj.cpf, obj.nome, obj.dataNascimento, obj.email, obj.senha });
                    var pessoa = ExecuteQuery<int>(@"SELECT max(id) FROM vini.D_Pessoa").FirstOrDefault();
                    //var pessoa = obj.Insert();

                    new PessoaAcesso().Inserir(pessoa, 1);
                    //new PessoaAcesso
                    //{
                    //    pessoaId = (int)pessoa,
                    //    acessoId = 1
                    //}.Insert();
                    ExecuteQuery(@"INSERT INTO vini.D_Cliente (pessoaId) VALUES (@pessoa)", new { pessoa });
                    //new Cliente
                    //{
                    //    pessoaId = (int)pessoa
                    //}.Insert();
                    message = "Usuário cadastrado com sucesso!";
                } catch(Exception ex)
                {
                    message = ex.Message;
                }
            }
            return message;
        }

        public Pessoa SelecionarPorId(int id)
        {
            return ExecuteQuery<Pessoa>(@"SELECT * FROM vini.D_Pessoa WHERE id = @id", new { id }).FirstOrDefault();
        }

        public List<Pessoa> SelecionarPorCpf(string cpf)
        {
            return ExecuteQuery<Pessoa>(@"SELECT * FROM vini.D_Pessoa WHERE cpf = @cpf", new { cpf });
        }

        public string AtualizarDados (Pessoa obj)
        {
            try
            {
                ExecuteQuery(@"UPDATE vini.D_Pessoa SET cpf = @cpf, nome = @nome, dataNascimento = @dataNascimento, email = @email,
                        senha = @senha WHERE id = @id", new { obj.cpf, obj.nome, obj.dataNascimento, obj.email, obj.senha, obj.id });
                return "Atualizado com sucesso!";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public string InserirFuncionario(Pessoa obj, int empresaId)
        {
            try
            {
                ExecuteQuery(@"INSERT INTO vini.D_Pessoa (cpf,nome,dataNascimento,email,senha) VALUES (@cpf,@nome,@dataNascimento,@email,@senha)", new { obj.cpf, obj.nome, obj.dataNascimento, obj.email, obj.senha });
                var pessoa = ExecuteQuery<int>(@"SELECT max(id) FROM vini.D_Pessoa").FirstOrDefault();
                new PessoaAcesso().Inserir(pessoa, 2);
                ExecuteQuery(@"INSERT INTO vini.D_Funcionario (pessoaId,empresaId) VALUES (@pessoa, @empresaId)", new { pessoa, empresaId });
                message = "Funcionário cadastrado com sucesso!";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return message;
        }
    }
}
