﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_TipoPagamento")]
    public class TipoPagamento : DefaultDatabase<TipoPagamento>
    {
        public int id { get; set; }
        public string nome { get; set; }
    }
}
