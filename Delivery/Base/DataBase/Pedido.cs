﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Pedido")]
    public class Pedido : DefaultDatabase<Pedido>
    {
        public int id { get; set; }
        public int pessoaId { get; set; }
        public int empresaId { get; set; }
        public int produtoId { get; set; }
        public double valor { get; set; }
        public int statusId { get; set; }


        public string AdicionarDados(Pedido obj)
        {
            try
            {
                ExecuteQuery(@"INSERT INTO vini.D_Pedido (pessoaId, empresaId, produtoId, valor, statusId, categoriaId, valorPromocional)
                                VALUES(@pessoaId, @empresaId, @produtoId, @valor, @statusId)",
                                new { obj.pessoaId, obj.empresaId, obj.produtoId, obj.valor, obj.statusId });
                return "Pedido realizado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string AtualizarDados(Pedido obj)
        {
            try
            {
                ExecuteQuery(@"UPDATE vini.D_Pedido SET pessoaId = @pessoaId, empresaId = @empresaId, produtoId = @produtoId, valor = @valor, 
                                statusId = @statusId WHERE id = @id",
                            new { obj.pessoaId, obj.empresaId, obj.produtoId, obj.valor, obj.statusId, obj.id });
                return "Pedido atualizado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Pedido> ListarPorEmpresas(int empresaId)
        {
            return ExecuteQuery<Pedido>(@"SELECT * FROM vini.D_Pedido WHERE empresaId = @empresaId", new { empresaId });
        }
    }
}
