﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Funcionario")]
    public class Funcionario : DefaultDatabase<Funcionario>
    {
        public int id { get; set; }
        public int pessoaId { get; set; }
    }
}
