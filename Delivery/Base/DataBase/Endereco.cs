﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataBase
{
    [Table("D_Endereco")]
    public class Endereco : DefaultDatabase<Endereco>
    {
        public int id { get; set; }
        public int pessoaId { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string estado { get; set; }
        public string cidade { get; set; }

        public string AdicionarDados(Endereco obj)
        {
            try
            {
                ExecuteQuery(@"INSERT INTO vini.D_Endereco (pessoaId, logradouro, numero, complemento, estado, cidade) 
                            VALUES (@pessoaId, @logradouro, @numero, @complemento, @estado, @cidade)", new { obj.pessoaId, obj.logradouro, obj.numero, obj.complemento, obj.estado, obj.cidade });
                return "Adicionado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string AtualizarDados(Endereco obj)
        {
            try
            {
                ExecuteQuery(@"UPDATE vini.D_Endereco SET pessoaId = @pessoaId, logradouro = @logradouro, numero = @numero, 
                            complemento = @complemento, estado = @estado, cidade = @cidade WHERE id = @id",
                            new { obj.pessoaId, obj.logradouro, obj.numero, obj.complemento, obj.estado, obj.cidade, obj.id });
                return "Atualizado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Endereco> ListarEnderecoPorId(int pessoaId)
        {
            return ExecuteQuery<Endereco>(@"SELECT * FROM vini.D_Endereco WHERE pessoaId = @pessoaId", new { pessoaId });
        }
    }
}
