﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Entrega")]
    public class Entrega : DefaultDatabase<Entrega>
    {
        public int id { get; set; }
        public int pessoaId { get; set; }
        public int pedidoId { get; set; }
        public int statusId { get; set; }
    }
}
