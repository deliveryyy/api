﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Cupom")]
    public class Cupom : DefaultDatabase<Cupom>
    {
        public int id { get; set; }
        public string nome { get; set; }
        public double valor { get; set; }
        public int valido { get; set; }
    }
}
