﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("vini.D_Status")]
    public class Status : DefaultDatabase<Status>
    {
        public int id { get; set; }
        public string nome { get; set; }

        public List<Status> Listar()
        {
            return ExecuteQuery<Status>(@"SELECT * FROM vini.D_Status");
        }
    }
}
