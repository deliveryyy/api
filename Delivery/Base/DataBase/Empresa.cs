﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Empresa")]
    public class Empresa : DefaultDatabase<Empresa>
    {
        public int id { get; set; }
        public string razaosocial { get; set; }
        public string cnpj { get; set; }
        public string nomefantasia { get; set; }
        public float valorentrega { get; set; }
        public string descricao { get; set; }

        public Empresa SelecionarPorId(int id)
        {
            return ExecuteQuery<Empresa>(@"SELECT * FROM vini.D_Empresa WHERE id = @id", new { id }).FirstOrDefault();
        }

        public string AdicionarDados(Empresa obj)
        {
            try
            {
                ExecuteQuery(@"INSERT INTO vini.D_Empresa (razaosocial, cnpj, nomefantasia, valorentrega, descricao) 
                                VALUES(@razaosocial, @cnpj, @nomefantasia, @valorentrega, @descricao)",
                    new { obj.razaosocial, obj.cnpj, obj.nomefantasia, obj.valorentrega, obj.descricao });
                return "Adicionado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string AtualizarDados(Empresa obj)
        {
            try
            {
                ExecuteQuery(@"UPDATE vini.D_Empresa SET razaosocial = @razaosocial, cnpj = @cnpj, nomefantasia = @nomefantasia, valorentrega = @valorentrega, descricao = @descricao  WHERE id = @id",
                            new { obj.razaosocial, obj.cnpj, obj.nomefantasia, obj.valorentrega, obj.descricao, obj.id });
                return "Atualizado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Empresa> ListarEmpresas()
        {
            return ExecuteQuery<Empresa>(@"SELECT * FROM vini.D_Empresa");
        }
    }
}
