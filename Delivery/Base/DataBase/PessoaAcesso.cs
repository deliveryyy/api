﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_PessoaAcesso")]
    public class PessoaAcesso : DefaultDatabase<PessoaAcesso>
    {
        public int id { get; set; }
        public int pessoaId { get; set; }
        public int acessoId { get; set; }


        public int Inserir(int pessoa, int nivel)
        {
            return ExecuteQuery<int>(@"INSERT INTO vini.D_PessoaAcesso (pessoaId, acessoId) VALUES (@pessoa, @nivel)", new { pessoa, nivel }).FirstOrDefault();
        }
    }
}
