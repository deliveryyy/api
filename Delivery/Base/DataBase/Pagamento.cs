﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Pagamento")]
    public class Pagamento : DefaultDatabase<Pagamento>
    {
        public int id { get; set; }
        public int tipoPagamentoId { get; set; }
        public int empresaId { get; set; }
        public double valor { get; set; }
        public int statusId { get; set; }
        public int cupomId { get; set; }
    }
}
