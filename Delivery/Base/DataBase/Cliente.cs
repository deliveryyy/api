﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Cliente")]
    public class Cliente : DefaultDatabase<Cliente>
    {
        public int id { get; set; }
        public int pessoaId { get; set; }

        //public List<Cliente> Listar()
        //{
        //    return ExecuteQuery<Cliente>(@"SELECT * FROM vini.Cliente");
        //}
    }
}
