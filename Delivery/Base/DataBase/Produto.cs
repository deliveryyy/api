﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("D_Produto")]
    public class Produto : DefaultDatabase<Produto>
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public double valor { get; set; }
        public int valido { get; set; }
        public int empresaId { get; set; }
        public int categoriaId{ get; set; }
        public double valorPromocional { get; set; }


        public Produto SelecionarPorId(int id)
        {
            return ExecuteQuery<Produto>(@"SELECT * FROM vini.D_Produto WHERE id = @id", new { id }).FirstOrDefault();
        }

        public string AdicionarDados(Produto obj)
        {
            try
            {
                ExecuteQuery(@"INSERT INTO vini.D_Produto (nome, descricao, valor, valido, empresaId, categoriaId, valorPromocional) VALUES(@nome, @descricao, @valor, @valido, @empresaId, @categoriaId, @valorPromocional)", 
                                new { obj.nome, obj.descricao, obj.valor, obj.valido, obj.empresaId, obj.categoriaId, obj.valorPromocional });
                return "Adicionado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string AtualizarDados(Produto obj)
        {
            try
            {
                ExecuteQuery(@"UPDATE vini.D_Produto SET nome = @nome, descricao = @descricao, valor = @valor, valido = @valido, 
                                empresaId = @empresaId, categoriaId = @categoriaId, valorPromocional = @valorPromocional WHERE id = @id",
                            new { obj.nome, obj.descricao, obj.valor, obj.valido, obj.empresaId, obj.categoriaId, obj.valorPromocional, obj.id });
                return "Atualizado com sucesso!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #region === Listas de Produtos ===
        public List<Produto> SelecionarPorEmpresa(int empresaId)
        {
            return ExecuteQuery<Produto>(@"SELECT * FROM vini.D_Produto WHERE empresaId = @empresaId AND valido = 1", new { empresaId });
        }

        public List<Produto> SelecionarPorCategoria(int categoriaId)
        {
            return ExecuteQuery<Produto>(@"SELECT * FROM vini.D_Produto WHERE categoriaId = @categoriaId AND valido = 1", new { categoriaId });
        }
        #endregion

    }
}
