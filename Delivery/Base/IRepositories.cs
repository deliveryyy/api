﻿using System.Collections.Generic;
using Dapper;
namespace Base
{

    internal interface IRepositories<TEntity> where TEntity : class
    {
        List<TEntity> All();
        TEntity Get(int id);
        int? Insert(int? timeout = null);
        int Update();
        int Delete();
        List<T> ExecuteQuery<T>(string query, object param = null, int? timeout = 60);
        List<T> GetByParams<T>(object whereConditions);
        int ExecuteStoredProcedure(string storedProcedure, DynamicParameters parameters = null, int? timeout = null);
        object GetPropertyValue(string propertyName);
        void SetPropertyValue(string propertyName, object value);
    }
}
