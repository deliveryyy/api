﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using Base.DataBase;

namespace MercadInApis.Controllers
{
    public class EmpresaController : Controller
    {
        [HttpGet]
        public ActionResult Detalhe(int id)
        {
            return Json(new Empresa().SelecionarPorId(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Adicionar(Empresa obj)
        {
            if (obj.id == 0)
                return Json(new Empresa().AdicionarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível adicionar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Editar(Empresa obj)
        {
            if (obj.id != 0)
                return Json(new Empresa().AtualizarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível atualizar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ListarEmpresas()
        {
            return Json(new Empresa().ListarEmpresas(), JsonRequestBehavior.AllowGet);
        }
    }
}