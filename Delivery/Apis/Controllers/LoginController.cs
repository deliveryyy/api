﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Base.DataBase;
using System.Net.Http;

namespace MercadInApis.Controllers
{
    public class LoginController : Controller
    {
        [HttpPost]
        public ActionResult Acessar(string email, string senha)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(senha))
                return Json("Campos inválidos!", JsonRequestBehavior.AllowGet);
            return Json(new Pessoa().VerificarAcessar(email, senha), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return null;
        }

        [HttpPost]
        public ActionResult Cadastrar(Pessoa obj)
        {
            return Json(new Pessoa().InserirCliente(obj), JsonRequestBehavior.AllowGet);
        }
    }
}