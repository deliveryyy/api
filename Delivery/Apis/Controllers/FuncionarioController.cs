﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Base.DataBase;

namespace MercadInApis.Controllers
{
    public class FuncionarioController : Controller
    {
        [HttpPost]
        public ActionResult Acessar(string email, string senha)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(senha))
                return Json("Campos inválidos!", JsonRequestBehavior.AllowGet);
            return Json(new Pessoa().VerificarAcessar(email, senha), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Cadastrar(Pessoa obj, int empresaId)
        {
            return Json(new Pessoa().InserirFuncionario(obj, empresaId), JsonRequestBehavior.AllowGet);
        }
    }
}