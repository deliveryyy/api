﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Base.DataBase;

namespace MercadInApis.Controllers
{
    public class PedidoController : Controller
    {
        [HttpGet]
        public ActionResult ListarStatusPedido()
        {
            return Json(new Status().Listar(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Adicionar(Pedido obj)
        {
            if (obj.id == 0)
                return Json(new Pedido().AdicionarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível adicionar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Editar(Pedido obj)
        {
            if (obj.id != 0)
                return Json(new Pedido().AtualizarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível atualizar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ListarPedidosPorEmpresa(int empresaId)
        {
            return Json(new Pedido().ListarPorEmpresas(empresaId), JsonRequestBehavior.AllowGet);
        }
    }
}