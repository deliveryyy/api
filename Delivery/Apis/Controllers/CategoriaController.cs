﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using Base.DataBase;

namespace MercadInApis.Controllers
{
    public class CategoriaController : Controller
    {
        [HttpPost]
        public ActionResult Adicionar(Categoria obj)
        {
            if (obj.id == 0)
                return Json(new Categoria().AdicionarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível adicionar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Editar(Categoria obj)
        {
            if (obj.id != 0)
                return Json(new Categoria().AtualizarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível atualizar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ListarCategorias()
        {
            return Json(new Categoria().ListarCategorias(), JsonRequestBehavior.AllowGet);
        }
    }
}