﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using Base.DataBase;

namespace MercadInApis.Controllers
{
    public class ProdutoController : Controller
    {
        [HttpGet]
        public ActionResult Detalhe(int id)
        {
            return Json(new Produto().SelecionarPorId(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Adicionar(Produto obj)
        {
            if (obj.id == 0)
                return Json(new Produto().AdicionarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível adicionar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Editar(Produto obj)
        {
            if (obj.id != 0)
                return Json(new Produto().AtualizarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível atualizar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ListarPorEmpresa(int empresaId)
        {
            return Json(new Produto().SelecionarPorEmpresa(empresaId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ListarPorCategoria(int categoriaId)
        {
            return Json(new Produto().SelecionarPorCategoria(categoriaId), JsonRequestBehavior.AllowGet);
        }
    }
}