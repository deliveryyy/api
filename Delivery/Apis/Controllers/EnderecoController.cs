﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using Base.DataBase;

namespace MercadInApis.Controllers
{
    public class EnderecoController : Controller
    {
        [HttpPost]
        public ActionResult Adicionar(Endereco obj)
        {
            if (obj.id == 0)
                return Json(new Endereco().AdicionarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível adicionar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Editar(Endereco obj)
        {
            if (obj.id != 0)
                return Json(new Endereco().AtualizarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível atualizar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarEnderecoPorId(int pessoaId)
        {
            return Json(new Endereco().ListarEnderecoPorId(pessoaId), JsonRequestBehavior.AllowGet);
        }
    }
}