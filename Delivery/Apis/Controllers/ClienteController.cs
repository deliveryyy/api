﻿using Base.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MercadInApis.Controllers
{
    public class ClienteController : Controller
    {
        [HttpGet]
        public ActionResult Detalhe(int id)
        {
            return Json(new Pessoa().SelecionarPorId(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Editar(Pessoa obj)
        {
            if (obj.id != 0)
                return Json(new Pessoa().AtualizarDados(obj), JsonRequestBehavior.AllowGet);
            else
                return Json("Não foi possível atualizar!\nTente Novamente mais tarde!", JsonRequestBehavior.AllowGet);
        }
    }
}